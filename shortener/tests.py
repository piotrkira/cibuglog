from django.test import TestCase
from django.utils import timezone
from shortener.models import Shortener


class ShortenerTests(TestCase):
    def setUp(self):
        self.full_value = '/freaking-long-path'

    def _basic_checks(self, s):
        # Check the returned instance
        self.assertEqual(s.full, self.full_value)
        self.assertAlmostEqual(s.added_on.timestamp(), timezone.now().timestamp(), places=0)

        # Check that the database has the same view
        db_s = Shortener.objects.get(full=self.full_value)
        self.assertEqual(db_s.shorthand, s.shorthand)
        self.assertEqual(db_s.full, s.full)
        self.assertEqual(db_s.added_on, s.added_on)
        self.assertEqual(db_s.last_accessed, s.last_accessed)

    def test_get_or_create(self):
        s1 = Shortener.get_or_create(self.full_value)
        self._basic_checks(s1)
        self.assertIsNone(s1.last_accessed)

        s2 = Shortener.get_or_create(self.full_value)
        self._basic_checks(s2)
        self.assertIsNotNone(s2.last_accessed)
        self.assertAlmostEqual(s2.last_accessed.timestamp(), timezone.now().timestamp(), places=0)

        s3 = Shortener.get_or_create(self.full_value)
        self._basic_checks(s3)
        self.assertGreater(s3.last_accessed, s2.last_accessed)

    def test_str(self):
        # NOTE: This also serves as stability test for the hashing mechanism
        s = Shortener.get_or_create(self.full_value)
        self.assertEqual(str(s), "Short 'ef747b9b55fb716e94712e6397cce438ad11be64'")
