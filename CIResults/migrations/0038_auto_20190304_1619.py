# Generated by Django 2.1.5 on 2019-03-04 16:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CIResults', '0037_machine_color_hex'),
    ]

    operations = [
        migrations.AlterField(
            model_name='runconfig',
            name='added_on',
            field=models.DateTimeField(auto_now_add=True, db_index=True),
        ),
    ]
