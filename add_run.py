#!/usr/bin/env python3

from django.db import transaction
import django
import argparse
import sys

django.setup()

from CIResults.run_import import RunConfigResults, TestSuiteRunDef  # noqa
from CIResults.models import RunConfig  # noqa


@transaction.atomic
def delete_runconfig(runconfig):
    try:
        runcfg = RunConfig.objects.get(name=runconfig)
    except RunConfig.DoesNotExist:
        print("The runconfig '{}' does not exist. Aborting...".format(runconfig))
        return

    runcfg.delete()


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("--public", help="Should new tests/machines be considered public?",
                    default="none", choices=('none', 'machines', 'tests', 'both'))
parser.add_argument("--vetted", help="Should new tests/machines be considered vetted?",
                    default="none", choices=('none', 'machines', 'tests', 'both'))
parser.add_argument("runconfig_dir", help="Directory containing the runconfig results", nargs='?')

parser.add_argument("-d", "--delete", help="Delete the runconfig specified")
parser.add_argument("-n", "--name", help="Name of the runconfig. Has to be unique")
parser.add_argument("-u", "--url", help="HTTP link to a public URL describing the runconfig")
parser.add_argument("-U", "--result_url_pattern", help="Result URL pattern. Accepted keywords:"
                                                       "    {runconfig}: name of the runconfig; "
                                                       "    {testsuite_build}: build name of the testsuite; "
                                                       "    {run_id}: ID of the testsuite run; "
                                                       "    {test}: name of the test; "
                                                       "    {machine}: name of the machine",
                                                       default="")
parser.add_argument("-e", "--environment", help="Description of the environment used")
parser.add_argument("-b", "--build", dest="builds", type=str, action='append',
                    help="Builds used in the runconfig")
parser.add_argument("-t", "--tag", dest="tags", type=str, action='append',
                    help="Tag used to create the runconfig (cannot be modified after creation)")
parser.add_argument("-r", "--results", metavar="FILE",
                    help="File containing a list of results to import (use - to read from stdin). "
                    "The file should contain one entry per line, and each line containing the following fields: "
                    "TESTSUITE_BUILD RESULTS_FORMAT RESULTS_FORMAT_VERSION MACHINE RUN_ID RESULTS_PATH")
parser.add_argument("-q", "--quiet", help="Do not output progression messages", action="store_true")
parser.add_argument("-T", "--temporary", help="Create a temporary run (cannot be modified after creation)",
                    action="store_true")

args = parser.parse_args()

# Check if we need to delete a run
if args.delete is not None:
    delete_runconfig(args.delete)
    sys.exit(0)

# Read the list of results that need to be imported
results_list = []
if args.results is not None:
    results_list_file = open(args.results) if args.results != '-' else sys.stdin

    for i, line in enumerate(results_list_file):
        f = line.rstrip().split(" ")
        if len(f) < 6:
            print("ERROR at line {}: the line does not contain all the necessary fields".format(i))
            continue

        try:
            results_list.append(TestSuiteRunDef(testsuite_build=f[0],
                                                results_format=f[1],
                                                results_format_version=f[2],
                                                machine=f[3], ts_run_id=f[4],
                                                ts_run_path=" ".join(f[5:])))
        except ValueError as e:
            print("ERROR at line {}: {}".format(i, str(e)))

    if len(results_list) == 0:
        print("No results found in the results list. Abort...")
        sys.exit(1)

rc = RunConfigResults(runconfig_dir=args.runconfig_dir, name=args.name,
                      url=args.url, result_url_pattern=args.result_url_pattern,
                      environment=args.environment, builds=args.builds,
                      tags=args.tags, results=results_list,
                      temporary=args.temporary)
rc.commit_to_db(
    new_machines_public=(args.public == "machines" or args.public == "both"),
    new_tests_public=(args.public == "tests" or args.public == "both"),
    new_machines_vetted=(args.vetted == "machines" or args.vetted == "both"),
    new_tests_vetted=(args.vetted == "tests" or args.vetted == "both"),
    quiet=args.quiet)
