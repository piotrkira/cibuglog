.. _replication-ui:

Replication Script UI
=====================

It is advised to use the specially built UI for creating replication scripts, rather than adding them directly in
the admin view. This UI has several useful features that will be discussed here, as well as general usage information.


.. note::

  Only users with the change_replicationscript permission granted can create and edit replication scripts. Make
  sure to grant this permission to all appropriate users first.

Accessing the UI
----------------

The replication script UI is accessible via the Bugs dropdown tab at the top of the page. This will only visible if the
user is logged in and has the appropriate permissions.

Clicking on this will bring you to a view with a table of all existing replication scripts, as well as a button to create
a new replication script.


Script Fields
-------------

Every script needs a script body, a name, a source tracker, and a destination tracker defined. There is also a toggle
to either enable or disable the script. When disabled, the script will not be executed on any bugs. This allows the
script to be saved in the database while being worked on without actually executing it.

Script editor
-------------

The UI features a builtin script editor with syntax highlighting and the ability to be toggled for fullscreen editing.
There are three buttons below the editor. There are options for both importing and exporting the script in the editor.
This way the script can be maintained and worked on outside of the UI, if desired. The other button will open this documenation
in a new tab.

For specifics on the replication script API and how to construct the actual script, consult the documentation here:
:ref:`replication-doc`

Script Validation
-----------------

To aid in script development and validation there is a dry run feature built into the UI. Once you are ready to test your
script, click the 'Check script result' button which will perform a dry run execution.

This will do both syntactical and functional checking. If there are any syntactic errors with the script, these will
be presented in a pop up with the error. If there are no syntactic issues, the script will be executed against the bugs
from the specified source tracker in the CI Bug Log DB.

The results of the script execution will be displayed in a table on the page. This will list all bugs that would be replicated
by the script. It will list the bug id, status, title, priority, replication operation, fields to set, and any comments
that are set to be added.

The replication operation field will note if the operation is create or update, depending on whether a replicated bug
already exists. If it is an update operation, the field when clicked will show a dropdown with the id, information, and
link to the replicated bug.

The primary information that will be most useful in validation is in the fields to set and comments to add sections. These
will show the JSON data that will be parsed and used to create/update the specific bugs. This can be used to make sure the
formatting is correct, as well as the right values are being used.

.. note::

  There is planned work on improving the dry run table UI to present the JSON data in a more readable way, or at
  least a way to export the dry run data so it can be analyzed externally.
