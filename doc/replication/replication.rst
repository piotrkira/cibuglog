.. _replication-doc:

Replication Script
====================
Replication scripts allow for the creation and updating of bugs between a source and destination tracker, The script
engine executes the user created Python script, which determines whether bugs should be replicated and if so
what fields/values to set in the destination tracker.

.. _tracker-api:

Bug Tracker Formats/API
-----------------------
To allow for the most flexibility in bug replication, each bug tracker's create/update operations
are specific to that tracker. Below are the trackers supported in CI Bug Log and links to further
documenation specific to each. It is advised that you read through this page first to get an
understanding of the overall process.

* :ref:`jira-api-doc`
* :ref:`gitlab-api-doc`
* :ref:`bugzilla-api-doc`

Procedure
---------
The script is executed in a secure environment, and as such, all system calls are not allowed.
The python standard library is however usable (as long as it does not generate system calls),
but you cannot load most external modules.

The script must define the function 'replication_check'. This function determines whether the supplied bug
should be replicated and returns a dict defining the bug replication. The format of this will be defined
in the next section.

If the replication script is enabled in the database, than when a bug from the specified source
tracker is polled this replication_check method will be executed on that bug.

It is important to note that the replication check function takes the source bug that is to be checked, as
well as the replicated destination bug. If the bug has not previously been replicated, the dest_bug argument
will be an empty dictionary.

If you need to handle updating an already replicated bug differently than creating a new bug, you can check
if the dest_bug is not empty to determine what to return. This allows the script to handle each process
separately

Below is the function signature that should be used:

.. code-block:: python

  def replication_check(src_bug, dest_bug)

.. note::

  When creating a new replication script the script field will be prepopulated with a dummy script, as
  a very trivial example. You can simply delete it and leave the function signature.

Parameters:
  **src_bug: dict()**:    variable holding the current bug for evaluation. New comments added to
  the bug since last polling will also be accessible via the dictionary key 'new_comments'. See the `Bug Fields`_
  and `Bug Comment Fields`_ sections for information on the values.

  **dest_bug: dict()**:    variable holding the replicated bug. Will be empty dict if bug
  hasn't been already replicated

Returns:
  **dict()**: Defines fields used for replication. Consult the tracker specific information in the
  `Bug Tracker Formats/API`_ section for the proper formatting for your destination tracker.

Bug Fields
----------
The following fields are accessible in the src_bug and dest_bug parameters passed to replication_check

=============   ==============================
Fields          Data Type
=============   ==============================
bug_id          str
title           str
description     str
tracker         str
created         str (YYYY-MM-DD hh:mm:ss.sTZD)
updated         str (YYYY-MM-DD hh:mm:ss.sTZD)
polled          str (YYYY-MM-DD hh:mm:ss.sTZD)
closed          str (YYYY-MM-DD hh:mm:ss.sTZD)
creator         str
assignee        str
product         str
component       str
priority        str
severity        str
features        list[str]
platforms       list[str]
status          str
tags            list[str]
custom_fields   dict[str, str]
=============   ==============================

Custom Fields
^^^^^^^^^^^^^
Different BugTracker projects may use fields that aren't natively builtin to CI Bug Log's Bug objects. To support
these additional fields, there is a custom_fields dictionary that can store these. The values are stored using a
string key and string value only. Lists, sets, dicts, or other structures are not allowed as values.

For more information on configuring custom fields for your trackers consult the following models documentation.

* :py:obj:`~CIResults.models.BugTracker`: custom_fields_map field
* :py:obj:`~CIResults.models.Bug`: custom_fields field

For additional information regarding how these fields are handled for specific trackers, consult the tracker
specific replication pages: :ref:`tracker-api`

Bug Comment Fields
------------------
The source bug also includes new comments that can be replicated as well. These are stored in the source
bug dictionary under the key 'new_comments'. This contains a list of comments dicts with the following fields
for each comment.

=======     ==============================
Fields      Data Type
=======     ==============================
author      str
created     str (YYYY-MM-DD hh:mm:ss.sTZD)
body        str
=======     ==============================

Bug Replication Dictionary Format
---------------------------------

The returned dictionary uses the keys described below. Any unneeded fields can simply be omitted. If the bug should
not be replicated then return an empty dictionary.

  **set_fields:** A dictionary where keys represent bug's fields to be set in the destination bugtracker.

  **db_fields_update:** A dictionary where keys are the names of the destination bug's DB fields
  to be updated directly. NOTE: This is deprecated in favor of the below two options.

  **db_src_fields_update:** A dictionary where keys are the names of the source bug's DB fields
  to be updated directly.

  **db_dest_fields_update:** A dictionary where keys are the names of the destination bug's DB fields
  to be updated directly.

  **add_comments:** A list of comments (str) to be added to the bug.

Replication Script UI
---------------------

Information on the features and usage of the replication script UI can be found here: :ref:`replication-ui`
