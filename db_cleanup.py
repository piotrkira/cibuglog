#!/usr/bin/env python3

import django

django.setup()

from CIResults.models import RunConfig, TestResult, TestSuite, Test, Machine
from django.db.models.functions import Length
from django.db.models.aggregates import Count
from django.db.models import Sum, Q
from django.utils import timezone
from datetime import timedelta
from itertools import islice
import argparse
import math

def split(l, size):
    it = iter(l)
    return iter(lambda: tuple(islice(it, size)), ())

def prompt(msg):
    res = input("{} (y/N)".format(msg))
    return res.lower() == "y"

def days_ago(days):
    return timezone.now() - timedelta(days=days)

# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("-t", "--del-tmp-runs", type=int, default=7,
                    help="Remove temporary runs older than the specified amount of days")
parser.add_argument("-c", "--compress-passes", type=int, default=-1,
                    help="Replace by NULL the stdout/stderr/dmesg of acceptable test results of runs older than the specified amount of days")
parser.add_argument("-l", "--del-lasting-runs", type=int, default=365,
                    help="Remove all runs older than the specified amount of days")
parser.add_argument("-s", "--compress-batch-size", type=int, default=1000,
                    help="Remove all runs older than the specified amount of days")
parser.add_argument("-r", "--remove-unused", type=int, default=-1,
                    help="Remove all unreferenced machines and test older than the specified amount of days")

parser.add_argument("-y", "--yes", help="Do not prompt for validation", action="store_true")

# TODO: Allow removing unused machines and tests

args = parser.parse_args()

# delete the old runs
old_tmp_runs = list(RunConfig.objects.filter(temporary=True, added_on__lt=days_ago(args.del_tmp_runs)).order_by('added_on'))
old_lasting_runs = list(RunConfig.objects.filter(temporary=False, added_on__lt=days_ago(args.del_lasting_runs)).order_by('added_on'))
msg = "Are you sure you want to remove {} temporary runs (up to '{}'), and {} lasting runs (up to '{}')?"
msg = msg.format(len(old_tmp_runs), old_tmp_runs[-1] if len(old_tmp_runs) > 0 else "N/A",
                 len(old_lasting_runs), old_lasting_runs[-1] if len(old_lasting_runs) > 0 else "N/A")
if args.yes or prompt(msg):
    to_delete = list(old_tmp_runs) + list(old_lasting_runs)
    for i, run in enumerate(to_delete):
        print("Deleting run {}/{}".format(i + 1, len(to_delete)))
        run.delete()
    print("")

# compress the passes
if args.compress_passes > 0:
    # get the list of acceptable passes
    acceptable_status = set()
    for testsuite in TestSuite.objects.all().prefetch_related('acceptable_statuses__testsuite'):
        for status in testsuite.acceptable_statuses.exclude(pk=testsuite.notrun_status_id):
            acceptable_status.add(status)

    compress_passes_up_to = days_ago(args.compress_passes)
    newest = RunConfig.objects.filter(added_on__lt=compress_passes_up_to).last()
    if newest is None:
        print('No results older than {} found. Aborting compression...'.format(compress_passes_up_to))
    else:
        msg = "Are you sure you want to permanently compress acceptable test results ({}) for results up to {}?"
        if args.yes or prompt(msg.format(", ".join([str(a) for a in acceptable_status]), newest)):
            print("Fetching the acceptable results older than {} days...".format(args.compress_passes))
            to_compress = TestResult.objects.filter(ts_run__runconfig__added_on__lt=compress_passes_up_to,
                                                    status__in=acceptable_status)
            to_compress = to_compress.exclude(Q(stdout=None) & Q(stderr=None) & Q(dmesg=None))
            to_compress = to_compress.values_list('id', flat=True)

            shard_count = math.ceil(len(to_compress) / args.compress_batch_size)
            for i, shard in enumerate(split(to_compress, args.compress_batch_size)):
                print("Compressing shard {}/{}".format(i+1, shard_count), end='\r')
                TestResult.objects.filter(pk__in=shard).update(stdout=None, stderr=None, dmesg=None)
            print("")

# Remove the tests and machines that did not participate in any run and which are older than specified by the user
if args.remove_unused >= 0:
    del_thrs = days_ago(args.remove_unused)
    to_del = Test.objects.annotate(results_count=Count('testresult')).filter(results_count=0,
                                                                            added_on=del_thrs)
    if len(to_del) > 0:
        msg = "The following {} tests are about to be deleted:\n{}\nAre you sure you want to continue?"
        msg = msg.format(len(to_del), ", ".join([str(t) for t in to_del]))
        if args.yes or prompt(msg):
            Test.objects.filter(pk__in=[t.pk for t in to_del]).delete()
            print("Deletion done\n")
    else:
        print('No unused tests found\n')

    to_del = Machine.objects.annotate(tsr_count=Count('testsuiterun'),
                                    aliased_count=Count('machine')).filter(tsr_count=0, aliased_count=0,
                                                                            added_on__lt=del_thrs)
    if len(to_del) > 0:
        msg = "The following {} machines are about to be deleted:\n{}\nAre you sure you want to continue?"
        msg = msg.format(len(to_del), ", ".join([m.name for m in to_del]))
        if args.yes or prompt(msg):
            Machine.objects.filter(pk__in=[m.pk for m in to_del]).delete()
            print("Deletion done\n")
    else:
        print('No unused machines found\n')
